# interview-frontend
Base for interview @Hello

The goal of this interview project is to build a simple todo list app using our stack.
We're using vuetify which comes as a dependency to this repo.


### Notes:
* You can use google Keep's todo-list as an example
* No need to do any backend integration, you can host the data locally
* No need to persist anything to localstorage

### Features
* Add / edit / delete tasks
* Mark a task as completed (checkboxes)
* Completed tasks should sorted to bottom
* Use vuetify components whenever possible
### Extras
* Responsiveness
* Nested task
* Drag n drop to re-order

Fork this repo and submit a PR when you're done :)



## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
